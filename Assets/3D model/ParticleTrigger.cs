using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ParticleTrigger : MonoBehaviour
{
    [SerializeField] float TextWaitTime;

    [SerializeField] float TextRunTime;

    // [SerializeField] float TextWaitFadeOutTime;

    [SerializeField] float TextFadeOutTime;

    [SerializeField] Vector3 StartScale;

    [SerializeField] Vector3 EndScale;

    [SerializeField] Transform Text;

    [Header("Play Fade Effect Time")]

    // [SerializeField] int LoopTextTime;

    // [SerializeField] int DelayTextTime;

    [Header("Setting Particle Fade")]

    [SerializeField] Vector3 ParticleEndScale;

    private List<ParticleSystem.MinMaxCurve> particleStartSize = new List<ParticleSystem.MinMaxCurve>();

    //[SerializeField] Color32 StartTextColor;

    //[SerializeField] Color32 EndTextColor;


    [SerializeField] ParticleSystem[] ps;

    private DateTime curTime;

    private Coroutine textEffect;

    private void OnDisable()
    {
        isActive = true;
        // for (int i = 0; i < ParticleWithShader.Length; i++)
        // {
        //     var main = ParticleWithShader[i].GetComponent<ParticleSystemRenderer>();
        //     main.material.SetFloat(ParticleReferenceProperty, ParticleStartAlpha);
        // }
        for (int i = 0; i < ps.Length; i++)
        {
            var main = ps[i].main;
            main.startSize = particleStartSize[i];
        }
        Text.localScale = StartScale;

    }
    bool isActive = true;
    private void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            if (isActive)
            {
                StopAllCoroutines();
                StartCoroutine(GetRequest("https://worldtimeapi.org/api/timezone/Asia/Singapore"));
                //StartCoroutine(ParticleEffect());
                textEffect = StartCoroutine(TextEffect());
            }
            else
            {
                if (particleStartSize.Count < 1)
                {
                    for (int i = 0; i < ps.Length; i++)
                    {
                        var main = ps[i].main;
                        particleStartSize.Add(main.startSize);
                    }
                }
            }
        }
        //psMain.Play();
        // StartCoroutine(TextEffect());

    }

    //IEnumerator TextEffect()
    //{
    //    isActive = false;
    //    yield return new WaitForSeconds(TextWaitTime);
    //    float time = 0;
    //    while (time < TextRunTime)
    //    {
    //        time += Time.deltaTime;
    //        Text.localScale = Vector3.Lerp(StartScale, EndScale, time / TextRunTime);
    //        yield return null;
    //    }
    //    Text.localScale = EndScale;
    //    DateTime PlayFadeEffectTime = new DateTime(Year, Month, Day, Hour, Minute, Second);
    //    float ParticleWaitTime = (float)(PlayFadeEffectTime - DateTime.Now).TotalSeconds;
    //    if(ParticleWaitTime > 0)
    //    {
    //        if (TextWaitFadeOutTime < ParticleWaitTime)
    //        {
    //            yield return new WaitForSeconds(TextWaitFadeOutTime);
    //        }
    //        else
    //        {
    //            yield return new WaitForSeconds(ParticleWaitTime);
    //        }
    //    }
    //    time = 0;
    //    while (time < TextFadeOutTime)
    //    {
    //        time += Time.unscaledDeltaTime;
    //        Text.localScale = Vector3.Lerp(EndScale, StartScale, time / TextFadeOutTime);
    //        yield return null;
    //    }
    //}

    IEnumerator GetRequest(string uri)
    {
        string timeText = "";
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.Success:
                    timeText = webRequest.downloadHandler.text;
                    string[] splitText = timeText.Split(',');
                    if (splitText.Length >= 3)
                    {
                        timeText = splitText[2];
                        splitText = timeText.Split('"');
                        if (splitText.Length >= 2)
                        {
                            curTime = DateTime.Parse(splitText[3]);
                            //getTimeCompleteTime = Time.unscaledTime;
                        }
                    }
                    break;
                default:
                    curTime = DateTime.Now;
                    //getTimeCompleteTime = Time.unscaledTime;
                    break;
            }

        }
        if (curTime.Minute % 10 == 1 && curTime.Second == 0)
        {
            StopCoroutine(textEffect);
            Text.localScale = StartScale;
            for (int i = 0; i < ps.Length; i++)
            {
                ParticleSystem.MinMaxCurve minMaxCurve = new ParticleSystem.MinMaxCurve();
                var main = ps[i].main;
                minMaxCurve.constantMin = 0;
                minMaxCurve.constantMax = 0;
                main.startSize = minMaxCurve;
                ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ps[i].particleCount];
                ps[i].GetParticles(particles);
                if (particles.Length < 1)
                    continue;
                for (int j = 0; j < particles.Length; j++)
                {
                    particles[j].startSize = 0;
                }
                ps[i].SetParticles(particles);
            }
        }
        else if (curTime.Minute % 10 == 0)
        {
            if (curTime.Second + TextFadeOutTime > 60)
            {
                StopCoroutine(textEffect);
                StartCoroutine(FadeOutEffect(60 - curTime.Second));
                StartCoroutine(ParticleEffect(60 - curTime.Second));
            }
            else
            {
                yield return new WaitForSecondsRealtime(60 - curTime.Second - TextFadeOutTime);
                StopCoroutine(textEffect);
                StartCoroutine(FadeOutEffect(TextFadeOutTime));
                StartCoroutine(ParticleEffect(TextFadeOutTime));
            }
        }
        else
        {
            int fadeOutHour = curTime.Hour;
            int fadeOutMinute = curTime.Minute;
            int tem = fadeOutMinute / 10;
            tem += 1;
            if (tem >= 6)
            {
                fadeOutHour += 1;
                tem -= 6;
            }
            fadeOutMinute = tem * 10;
            DateTime fadeOutTime = new DateTime(curTime.Year, curTime.Month, curTime.Day, fadeOutHour, fadeOutMinute, 60 - (int)TextFadeOutTime);
            yield return new WaitForSecondsRealtime((float)(fadeOutTime - curTime).TotalSeconds);
            StopCoroutine(textEffect);
            StartCoroutine(FadeOutEffect(TextFadeOutTime));
            StartCoroutine(ParticleEffect(TextFadeOutTime));
        }
    }

    IEnumerator TextEffect()
    {
        isActive = false;
        yield return new WaitForSeconds(TextWaitTime);
        float time = 0;
        while (time < TextRunTime)
        {
            time += Time.deltaTime;
            Text.localScale = Vector3.Lerp(StartScale, EndScale, time / TextRunTime);
            yield return null;
        }
    }

    IEnumerator FadeOutEffect(float fadeOutTime)
    {
        float time = 0;
        while (time < fadeOutTime)
        {
            time += Time.unscaledDeltaTime;
            Text.localScale = Vector3.Lerp(EndScale, StartScale, time / fadeOutTime);
            yield return null;
        }
    }

    IEnumerator ParticleEffect(float fadeOutTime)
    {
        float time = 0;
        List<float> particleMinSize = new List<float>();
        List<float> particleMaxSize = new List<float>();
        for (int i = 0; i < ps.Length; i++)
        {
            var main = ps[i].main;
            particleMinSize.Add(main.startSize.constantMin);
            particleMaxSize.Add(main.startSize.constantMax);
        }
        while (time < fadeOutTime)
        {
            float preTime = time;
            time += Time.unscaledDeltaTime;
            for (int i = 0; i < ps.Length; i++)
            {
                ParticleSystem.MinMaxCurve minMaxCurve = new ParticleSystem.MinMaxCurve();
                var main = ps[i].main;
                minMaxCurve.constantMin = particleMinSize[i] - particleMinSize[i] * (time / fadeOutTime);
                minMaxCurve.constantMax = particleMaxSize[i] - particleMaxSize[i] * (time / fadeOutTime);
                main.startSize = minMaxCurve;
                ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ps[i].particleCount];
                ps[i].GetParticles(particles);
                if (particles.Length < 1)
                    continue;
                for (int j = 0; j < particles.Length; j++)
                {
                    float startSize = particles[j].startSize;
                    particles[j].startSize = startSize - startSize * (Time.unscaledDeltaTime / (fadeOutTime - preTime));
                }
                ps[i].SetParticles(particles);
            }
            yield return null;
        }
    }
}
