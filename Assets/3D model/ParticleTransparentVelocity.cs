using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTransparentVelocity : MonoBehaviour
{
    //[SerializeField] ParticleSystem psMain;

    // [SerializeField] float TextWaitTime;

    // [SerializeField] float TextRunTime;

    // [SerializeField] float TextFadeOutTime;

    // [SerializeField] float StartAlpha;

    [SerializeField] float FastTime;

    [SerializeField] float SpeedDownTime;

    [SerializeField] float StartSimulationSpeed;

    [SerializeField] float EndSimulationSpeed;

    // [SerializeField] string ReferenceProperty;

    //[SerializeField] Vector3 StartScale;

    //[SerializeField] Vector3 EndScale;

    // [SerializeField] Renderer Text;

    [SerializeField] float ParticleWaitTime;

    [SerializeField] float ParticleFadeOutTime;

    [SerializeField] float ParticleStartAlpha;

    [SerializeField] float ParticleEndAlpha;

    [SerializeField] string ParticleReferenceProperty;

    [SerializeField] ParticleSystem[] ParticleWithShader;

    // [SerializeField] Vector3 StartParticleScale;

    [SerializeField] ParticleSystem[] ParticleWithBillboard;

    private List<ParticleSystem.MinMaxCurve> particleStartSize = new List<ParticleSystem.MinMaxCurve>();

    // private void OnEnable()
    // {
    //     //psMain.Play();
    //     // StartCoroutine(TextEffect());
    //     for (int i = 0; i < ParticleWithShader.Length; i++)
    //     {
    //         var main = ParticleWithShader[i].GetComponent<ParticleSystemRenderer>();
    //         main.material.SetFloat(ParticleReferenceProperty, ParticleStartAlpha);
    //     }
    //     StartCoroutine(ParticleEffect());
    // }

    private void OnDisable()
    {
        isActive = true;
        for (int i = 0; i < ParticleWithShader.Length; i++)
        {
            var main = ParticleWithShader[i].GetComponent<ParticleSystemRenderer>();
            main.material.SetFloat(ParticleReferenceProperty, ParticleStartAlpha);
            var main2 = ParticleWithShader[i].main;
            main2.simulationSpeed = StartSimulationSpeed;
        }
        for (int i = 0; i < ParticleWithBillboard.Length; i++)
        {
            var main = ParticleWithBillboard[i].main;
            main.startSize = particleStartSize[i];
        }

    }

    // private void OnEnable()
    // {
    //     Debug.Log("OnEnable");
    // }

    bool isActive = true;
    private void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            if (isActive)
            {
                StopAllCoroutines();
                StartCoroutine(SpeedEffect());       
                StartCoroutine(ParticleEffect());
                // StartCoroutine(SpeedDownEffect());
            }
            else
            {
                if (particleStartSize.Count < 1)
                {
                    for (int i = 0; i < ParticleWithBillboard.Length; i++)
                    {
                        var main = ParticleWithBillboard[i].main;
                        particleStartSize.Add(main.startSize);
                    }
                }
            }
        }
        //psMain.Play();
        // StartCoroutine(TextEffect());

    }

    // IEnumerator TextEffect()
    // {
    //     Text.material.SetFloat(ReferenceProperty, StartAlpha);
    //     yield return new WaitForSeconds(TextWaitTime);
    //     float time = 0;
    //     while (time < TextRunTime)
    //     {
    //         time += Time.deltaTime;
    //         Text.material.SetFloat(ReferenceProperty, Mathf.Lerp(StartAlpha, EndAlpha, time / TextRunTime));
    //         yield return null;
    //     }
    //     yield return new WaitForSeconds(TextWaitFadeOutTime);
    //     time = 0;
    //     while (time < TextFadeOutTime)
    //     {
    //         time += Time.unscaledDeltaTime;
    //         Text.material.SetFloat(ReferenceProperty, Mathf.Lerp(EndAlpha, StartAlpha, time / TextFadeOutTime));
    //         yield return null;
    //     }
    // }

    IEnumerator SpeedEffect()
    {
        for (int i = 0; i < ParticleWithShader.Length; i++)
        {
            var main1 = ParticleWithShader[i].main;
            main1.simulationSpeed = StartSimulationSpeed;
        }
        for (int i = 0; i < ParticleWithBillboard.Length; i++)
        {
            var main2 = ParticleWithBillboard[i].main;
            main2.simulationSpeed = StartSimulationSpeed;
        }
        // yield return null;
    // }
    // IEnumerator SpeedDownEffect()
    // {
        float time1 = 0;
        yield return new WaitForSecondsRealtime(FastTime);
        while (time1 < SpeedDownTime) //FastTime*StartSimulationSpeed + (ParticleWaitTime-FastTime)*EndSimulationSpeed)
        {
            float preTime = time1;
            time1 += Time.unscaledDeltaTime;
            // Debug.Log(time1);
            for (int i = 0; i < ParticleWithShader.Length; i++)
            {
                var main1 = ParticleWithShader[i].main;
                main1.simulationSpeed = Mathf.Lerp(StartSimulationSpeed, EndSimulationSpeed, time1 / SpeedDownTime);
            }
            for (int i = 0; i < ParticleWithBillboard.Length; i++)
            {
                var main2 = ParticleWithBillboard[i].main;
                main2.simulationSpeed = Mathf.Lerp(StartSimulationSpeed, EndSimulationSpeed, time1 / SpeedDownTime);
            }

            yield return null;
        }
    }
    IEnumerator ParticleEffect()
    {
        //Debug.Log("StartParticle " + gameObject.name);
        // for (int i = 0; i < ParticleWithShader.Length; i++)
        // {
        //     var main2 = ParticleWithShader[i].main;
        //     main2.simulationSpeed = StartSimulationSpeed;
        // }
        // yield return new WaitForSecondsRealtime(FastTime);
        // for (int i = 0; i < ParticleWithShader.Length; i++)
        // {
        //     var main2 = ParticleWithShader[i].main;
        //     main2.simulationSpeed = EndSimulationSpeed;
        // }
        isActive = false;
        float time = 0;
        yield return new WaitForSecondsRealtime(ParticleWaitTime); //FastTime*StartSimulationSpeed + (ParticleWaitTime-FastTime)*EndSimulationSpeed);
        // while(time < ParticleWaitTime)
        // {
        //    time += Time.unscaledDeltaTime;
        //    Debug.Log("RunThis " + time);
        //    if (!gameObject.activeInHierarchy)
        //    {
        //        Debug.Log("ResetToTrue");
        //        isActive = true;
        //        yield break;
        //    }
        //    yield return null;
        // }
        //time = 0f;
        //yield return new WaitForSecondsRealtime(ParticleWaitTime);
        List<float> particleMinSize = new List<float>();
        List<float> particleMaxSize = new List<float>();
        for (int i = 0; i < ParticleWithShader.Length; i++)
        {
            var main = ParticleWithShader[i].main;
            particleMinSize.Add(main.startSize.constantMin);
            particleMaxSize.Add(main.startSize.constantMax);
        }
        while (time < ParticleWaitTime) //FastTime*StartSimulationSpeed + (ParticleWaitTime-FastTime)*EndSimulationSpeed)
        {
            //if (!gameObject.activeInHierarchy)
            //{
            //    Debug.Log("ResetToTrue");
            //    isActive = true;
            //    yield break;
            //}
            //Debug.Log("RunThat");
            float preTime = time;
            time += Time.unscaledDeltaTime;
            for (int i = 0; i < ParticleWithShader.Length; i++)
            {
                var main = ParticleWithShader[i].GetComponent<ParticleSystemRenderer>();
                main.material.SetFloat(ParticleReferenceProperty, Mathf.Lerp(ParticleStartAlpha, ParticleEndAlpha, time / ParticleFadeOutTime));
                ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ParticleWithBillboard[i].particleCount];
                ParticleWithBillboard[i].GetParticles(particles);
                if (particles.Length < 1)
                    continue;
                ParticleSystem.MinMaxCurve minMaxCurve = new ParticleSystem.MinMaxCurve();
                var main2 = ParticleWithBillboard[i].main;
                minMaxCurve.constantMin = particleMinSize[i] - particleMinSize[i] * (time / Time.unscaledDeltaTime);
                minMaxCurve.constantMax = particleMaxSize[i] - particleMaxSize[i] * (time / Time.unscaledDeltaTime);
                main2.startSize = minMaxCurve;
                for (int j = 0; j < particles.Length; j++)
                {
                    float startSize = particles[j].startSize;
                    particles[j].startSize = startSize - startSize * (Time.unscaledDeltaTime / (ParticleFadeOutTime - preTime));
                }
                ParticleWithBillboard[i].SetParticles(particles);
            }

            yield return null;
        }
    }
}
