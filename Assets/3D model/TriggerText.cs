using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerText : MonoBehaviour
{

    [SerializeField] Vector3 StartScale = Vector3.zero;

    [SerializeField] Vector3 EndScale = new Vector3(-7.007554f, 7.007554f, 0.004459f);

    [SerializeField] float ShowTime;

    [SerializeField] Transform Text;
 
    private void Start()
    {
        Text.localScale = Vector3.zero;
    }

    private void OnParticleTrigger()
    {
        StartCoroutine(ShowText());
    }

    IEnumerator ShowText()
    {
        float time = 0;
        while (time < ShowTime)
        {
            time += Time.deltaTime;
            Text.localScale = Vector3.Lerp(StartScale, EndScale, time / ShowTime);
            yield return null;
        }
    }
}
