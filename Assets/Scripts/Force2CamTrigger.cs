using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Force2CamTrigger : MonoBehaviour
{     
    // Floating time before numbers are sucked into camera
    public float loopTime = 20;

    // Sucking numbers into camera time
    public float suckTime = 5;

    // Final gravity value of external force
    public float finalForce = 0.5f;

    // Counter variables
    private float loopTimeValue;
    private float suckTimeValue;

    private ParticleSystemForceField force2Cam;

    // Start is called before the first frame update
    void Start()
    {
        force2Cam = this.transform.parent.GetChild(1).GetComponent<ParticleSystemForceField>();
        loopTimeValue = loopTime;
        suckTimeValue = suckTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (suckTimeValue > 0)
        {
            if (loopTimeValue > 0)
            {
                loopTimeValue -= Time.deltaTime;
            }
            else
            {
                suckTimeValue -= Time.deltaTime;
                force2Cam.gravity = (suckTime - suckTimeValue)*finalForce/suckTime;
            }
        }
        else
        {
            suckTimeValue += suckTime;
            loopTimeValue += loopTime;
            force2Cam.gravity = 0;
        }
    }
}
