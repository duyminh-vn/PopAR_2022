
// using System;
// using System.Collections;
// using UnityEngine;
// using UnityEngine.XR.ARSubsystems;
// using UnityEngine.XR.ARFoundation;
// using Google.XR.ARCoreExtensions;

// namespace JohnBui
// {
//     public class NotiController : MonoBehaviour
//     {

//         [SerializeField] ImageTargetController imageTargetController;

//         [SerializeField] ARTrackedImageManager m_TrackedImageManager;


//         [SerializeField] AREarthManager EarthManager;

//         [SerializeField] VpsInitializer Initializer;

//         public NotiManager notiManager;
//         // Start is called before the first frame update
//         public bool isTrack1 = false;
//         public bool isTrack2 = false;
//         public bool isTrack3 = false;
//         public bool isActivePopUpSuccessLate = false;

//         private bool isFirst = true;
//         private int firstSec = 0;
//         private int timeFirst = 10;

//         public string tracker1 = "track_m1";
//         public string tracker2 = "img-track";
//         public string tracker3 = "banner";

//         public string[] eventList;

//         public Animator eventAnimator;


//         void Start()
//         {
//             if (notiManager == null)
//             {
//                 Debug.Log("Error");
//             }
//             firstSec = DateTime.Now.Second;

//             isTrack1 = false;
//             isTrack2 = false;
//             isTrack3 = false;          
//         }

//         // Update is called once per frame
//         void Update()
//         {


//             DateTime time = DateTime.Now;
//             int currentMin = time.Minute % 10;
//             Debug.Log($"min: {currentMin}, second: {time.Second}");
//             //if (!Initializer.IsReady || EarthManager.EarthTrackingState != TrackingState.Tracking)
//             //{
//             //    notiManager.turnOffAll(false);
//             //}

//             //HD chup hinh {3,4}
//             //Point to {0}
//             //Success {1}
//             //Fail {2}

//             if (time.Second < firstSec + timeFirst && isFirst)
//             {
//                 isFirst = false;
//                 loadTrack(4);
//             }

//             trackMin(time);

//             if (currentMin == 0 && (time.Second >= 0 && time.Second <= 3) && !MessageManager.Instance.isActivePopUpSafe)
//             {
//                 // 0:00 bat lai point to
//                 MessageManager.Instance.isActivePopUpMessage = true;
//             }
//             else if (currentMin == 6 && (time.Second >= 0 && time.Second <= 3) && !MessageManager.Instance.isActivePopUpSafe)
//             {
//                 // 6:00 bat lai point to
//                 MessageManager.Instance.isActivePopUpMessage = true;
//             }

//             if (MessageManager.Instance.isActivePopUpMessage)
//             {
//                 //MessageManager.Instance.isActivePopUpMessage = false;
//                 //MessageManager.Instance.DeactivePopupFail();
//                 //MessageManager.Instance.DeactivePopupSuccess();
//                 //MessageManager.Instance.ActivePopupMessage();
//                 eventAnimator.Play("offAll");
//                 eventAnimator.Play("calibSucess");
//             }

//             if (!Initializer.IsReady || EarthManager.EarthTrackingState != TrackingState.Tracking)
//             {
//                 if (MessageManager.Instance.isCalibFail)
//                 {
//                     MessageManager.Instance.isCalibFail = false;
//                     if (currentMin == 1 && (time.Second >= 0 && time.Second <= 4) && !isTrack1)
//                     {
//                         Debug.Log("ko detect dc m1");
//                         // tu 00:00 den 00:59 khong detect thi 1:00 tat POINT TO bat FAIL
//                         //MessageManager.Instance.DeactivePopupMessage();
//                         //MessageManager.Instance.DeactivePopupSuccess();
//                         //MessageManager.Instance.ActivePopupFail();
//                         eventAnimator.Play("offAll");
//                         eventAnimator.Play("calibFail");
//                     }
//                     else if (currentMin == 7 && (time.Second >= 0 && time.Second <= 4) && !isTrack2)
//                     {
//                         Debug.Log("ko detect dc m2");
//                         // tu 06:00 den 06:59 khong detect thi 7:00 tat POINT TO bat FAIL
//                         //MessageManager.Instance.DeactivePopupMessage();
//                         //MessageManager.Instance.DeactivePopupSuccess();
//                         //MessageManager.Instance.ActivePopupFail();
//                         eventAnimator.Play("offAll");
//                         eventAnimator.Play("calibFail");

//                     }
//                     else
//                     {
//                         MessageManager.Instance.isCalibFail = true;
//                     }
//                 }
//             }
//             else
//             {


//                 foreach (var trackedImage in m_TrackedImageManager.trackables)
//                 {
//                     if (trackedImage.trackingState == TrackingState.Tracking)
//                     {
//                         if (trackedImage.referenceImage.name == tracker1)
//                         {
//                             if (currentMin == 0) isTrack1 = true;
//                         }
//                         if (trackedImage.referenceImage.name == tracker2)
//                         {
//                             if (currentMin == 6) isTrack2 = true;
//                         }
//                         if (trackedImage.referenceImage.name == tracker3)
//                         {
//                             isTrack3 = true;
//                         }
//                     }


//                     //if (trackedImage.trackingState == TrackingState.Limited)
//                     //{
//                     //    if (trackedImage.referenceImage.name == tracker1)
//                     //    {
//                     //        isTrack1 = false;
//                     //    }
//                     //    if (trackedImage.referenceImage.name == tracker2)
//                     //    {
//                     //        isTrack2 = false;
//                     //    }
//                     //    if (trackedImage.referenceImage.name == tracker3)
//                     //    {
//                     //        isTrack3 = false;
//                     //    }
//                     //}


//                     //trackMin(time);





//                     //if (currentMin == 0 && isTrack1)
//                     //{
//                     //    // 00:00 den 00:59 neu thanh cong tat POINT TO bat SUCCESS
//                     //    //isActivePopUpSuccessLate = true;
//                     //    //MessageManager.Instance.DeactivePopupMessage();
//                     //    //MessageManager.Instance.DeactivePopupFail();
//                     //    //MessageManager.Instance.ActivePopupSuccess();
//                     //    loadTrack(1);
//                     //}
//                     //else if (currentMin == 6 && isTrack2)
//                     //{
//                     //    // 06:00 den 06:59 neu thanh cong tat POINT TO bat SUCCESS
//                     //    //isActivePopUpSuccessLate = true;
//                     //    //MessageManager.Instance.DeactivePopupMessage();
//                     //    //MessageManager.Instance.DeactivePopupFail();
//                     //    //MessageManager.Instance.ActivePopupSuccess();

//                     //    loadTrack(1);
//                     //}
//                     //// neu trong vong 1 phut va 7 phut & chua hien SUCCESS & hien muon
//                     //else if ((currentMin == 1 || currentMin == 7)
//                     //&& isActivePopUpSuccessLate && !MessageManager.Instance.isActivePopupCalibSuccess)
//                     //{
//                     //    // hien muon sau 1 phut va 7 phut
//                     //    isActivePopUpSuccessLate = false;
//                     //    MessageManager.Instance.DeactivePopupMessage();
//                     //    MessageManager.Instance.DeactivePopupFail();
//                     //    MessageManager.Instance.ActivePopupSuccess();
//                     //}
//                     //else
//                     //{
//                     //    MessageManager.Instance.isCalibTrue = true;
//                     //}

//                     /* openApp
//                     calibMess
//                     calibSucess
//                     calibFail
//                     offAll
//                     */

//                     //LOGIC:

//                     if (currentMin == 0 && isTrack1)
//                     {
//                         // 00:00 den 00:59 neu thanh cong tat POINT TO bat SUCCESS
//                         isActivePopUpSuccessLate = true;
//                         //MessageManager.Instance.DeactivePopupMessage();
//                         //MessageManager.Instance.DeactivePopupFail();
//                         //MessageManager.Instance.ActivePopupSuccess();

//                         eventAnimator.Play("offAll");
//                         eventAnimator.Play("calibSucess");
//                     }
//                     else if (currentMin == 6 && isTrack2)
//                     {
//                         // 06:00 den 06:59 neu thanh cong tat POINT TO bat SUCCESS
//                         isActivePopUpSuccessLate = true;
//                         //MessageManager.Instance.DeactivePopupMessage();
//                         //MessageManager.Instance.DeactivePopupFail();
//                         //MessageManager.Instance.ActivePopupSuccess();
//                         eventAnimator.Play("offAll");
//                         eventAnimator.Play("calibSucess");

//                     }
//                     // neu trong vong 1 phut va 7 phut & chua hien SUCCESS & hien muon
//                     else if ((currentMin == 1 || currentMin == 7)
//                     && isActivePopUpSuccessLate && !MessageManager.Instance.isActivePopupCalibSuccess)
//                     {
//                         // hien muon sau 1 phut va 7 phut
//                         isActivePopUpSuccessLate = false;
//                         //MessageManager.Instance.DeactivePopupMessage();
//                         //MessageManager.Instance.DeactivePopupFail();
//                         //MessageManager.Instance.ActivePopupSuccess();
//                         eventAnimator.Play("offAll");
//                         eventAnimator.Play("calibSucess");
//                     }
//                     else
//                     {
//                         MessageManager.Instance.isCalibTrue = true;
//                     }


//                 }
//             }
//         }

//     public void trackMin(DateTime time)
//     {
//         Debug.Log("Call track Min");
//         //DateTime time = DateTime.Now;
//         int currentMin = time.Minute % 10;
//         if (currentMin == 0)
//         {
//             // 00:00 den 00:59
//             if ( time.Second < firstSec + timeFirst && isFirst)
//             {
//                 isFirst = false;
//                 loadTrack(4);
//             }
//             else
//             {
//                 if (isTrack1)
//                 {
//                     loadTrack(1);
//                 }
//                 else
//                 {
//                     loadTrack(0);
//                 }
//             }

//         }
//         if (currentMin >= 1 && currentMin < 6)
//         {
//         // 01:00 den 05:59
//             if (time.Second < firstSec + timeFirst && isFirst)
//             {
//                 isFirst = false;
//                 loadTrack(4);
//             }
//             else
//             {
//                 if (isTrack1)
//                 {
//                     notiManager.turnOffAll(false);
//                 }
//                 else
//                 {
//                     loadTrack(2);
//                 }
//             }
//         }
//         if (currentMin == 6)
//         {
//             // 06:00 den 06:59
//             if (time.Second < firstSec + timeFirst && isFirst)
//             {
//                 isFirst = false;
//                 loadTrack(4);
//             }
//             else
//             {
//                 if (isTrack2)
//                 {
//                     loadTrack(1);
//                 }
//                 else
//                 {
//                     loadTrack(0);
//                 }
//             }
//             }
//             if (currentMin >= 7)
//             {
//                 // 07:00 den 09:59
//                 if (time.Second < firstSec + timeFirst && isFirst)
//                 {
//                     isFirst = false;
//                     loadTrack(4);
//                 }
//                 else
//                 {
//                     if (isTrack2)
//                     {
//                         notiManager.turnOffAll(false);
//                     }
//                     else
//                     {
//                         loadTrack(2);
//                     }
//                 }
//             }
//         }


//         public void loadTrack(int index)
//         {
//             int[] paramsResult = new int[] { index };
//             //int[] intArray = new int[] { 3, 4, 5 };
//             switch (index)
//             {
//             case 1:
//                 Debug.Log($"Load track: {index}");
//                 //isTrack1 = true;
//                 notiManager.loadNotiByList(paramsResult);
//                 break;
//             case 2:
//                 Debug.Log($"Load track: {index}");
//                 //isTrack2 = true;
//                 notiManager.loadNotiByList(paramsResult);
//                 break;
//             case 3:
//                 Debug.Log($"Load track: {index}");
//                 //isTrack3 = true;
//                 notiManager.loadNotiByList(paramsResult);
//                 break;
//             case 4:
//                 Debug.Log($"Load track: {index}");
//                 //isTrack4 = true;
//                 int[] paramsM4 = new int[] { 3,4 };
//                 notiManager.loadNotiByList(paramsM4);
//                 break;
//             default:
//                 Debug.Log("No msg");
//                 break;
//             }
//         }
//     }
// }