using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageTargetNoTime : MonoBehaviour
{
    [SerializeField] Transform marker01_01, marker01_02;
    [SerializeField] Transform marker02;
    [SerializeField] Transform marker03;


    [SerializeField] GameObject objTarget01, objTarget01_02;
    [SerializeField] GameObject objTarget02;
    [SerializeField] GameObject objTarget03;

    [SerializeField] SpawnableManager spawnableManager;

    public bool isDetec = false;
    private void Start()
    {
        objTarget01.SetActive(false);
        objTarget01_02.SetActive(false);
        objTarget02.SetActive(false);
        objTarget03.SetActive(false);
    }

    private void Update()
    {
        DateTime now = DateTime.Now;
        CheckMarker01(now);
        CheckMarker02(now);
        CheckMarker03(now);
        if (objTarget01.activeInHierarchy && objTarget01_02.activeInHierarchy && objTarget02.activeInHierarchy && objTarget03.activeInHierarchy)
        {
            isDetec = true;
        }
        else
        {
            isDetec = false;
        }
    }

    bool sttMark1 = true;
    void CheckMarker01(DateTime time)
    {
        if (marker01_01.gameObject.activeInHierarchy)
        {
            if (sttMark1)
            {
                if (spawnableManager.spawnedObject != null)
                {
                    Destroy(spawnableManager.spawnedObject);
                    spawnableManager.SpawnPrefab(marker01_01.position);
                }
                else
                {
                    spawnableManager.SpawnPrefab(marker01_01.position);
                }
                objTarget01.SetActive(true);
                StartCoroutine(Count1Min());
                sttMark1 = false;
                sttMark2 = true;
                sttMark3 = true;
            }
            spawnableManager.spawnedObject.transform.position = marker01_01.position;
            spawnableManager.spawnedObject.transform.rotation = marker01_01.rotation;

            objTarget01.transform.position = spawnableManager.spawnedObject.transform.position;
            objTarget01.transform.rotation = spawnableManager.spawnedObject.transform.rotation;
            

            objTarget01_02.SetActive(false);
            objTarget02.SetActive(false);
            objTarget03.SetActive(false);
        }
    }

    bool sttMark2 = true;
    void CheckMarker02(DateTime time)
    {
        if (marker02.gameObject.activeInHierarchy)
        {
            if (sttMark2)
            {
                if (spawnableManager.spawnedObject != null)
                {
                    Destroy(spawnableManager.spawnedObject);
                    spawnableManager.SpawnPrefab(marker02.position);
                }
                else
                {
                    spawnableManager.SpawnPrefab(marker02.position);
                }
                objTarget02.SetActive(true);
                sttMark2 = false;
                sttMark1 = true;
                sttMark3 = true;
            }

            spawnableManager.spawnedObject.transform.position = marker02.position;
            spawnableManager.spawnedObject.transform.rotation = marker02.rotation;

            objTarget02.transform.position = spawnableManager.spawnedObject.transform.position;
            objTarget02.transform.rotation = spawnableManager.spawnedObject.transform.rotation;

            objTarget01.SetActive(false);
            objTarget01_02.SetActive(false);
            objTarget03.SetActive(false);
        }
    }




    bool sttMark3 = true;
    void CheckMarker03(DateTime time)
    {
        if (marker03.gameObject.activeInHierarchy)
        {
            if (sttMark3)
            {
                if (spawnableManager.spawnedObject != null)
                {
                    Destroy(spawnableManager.spawnedObject);
                    spawnableManager.SpawnPrefab(marker03.position);
                }
                else
                {
                    spawnableManager.SpawnPrefab(marker03.position);
                }
                sttMark3 = false;
                sttMark1 = true;
                sttMark2 = true;
            }

            objTarget03.SetActive(true);

            spawnableManager.spawnedObject.transform.position = marker03.position;
            spawnableManager.spawnedObject.transform.rotation = marker03.rotation;

            objTarget03.transform.position = spawnableManager.spawnedObject.transform.position;
            objTarget03.transform.rotation = spawnableManager.spawnedObject.transform.rotation;

            objTarget01.SetActive(false);
            objTarget01_02.SetActive(false);
            objTarget02.SetActive(false);
        }
    }

    int i = 0;
    IEnumerator Count1Min()
    {
        yield return new WaitForSeconds(1);
        i += 1;
        if (i < 30)
        {
            StartCoroutine(Count1Min());
        }
        else if (i == 30)
        {
            objTarget01.SetActive(false);
            objTarget01_02.SetActive(true);
            spawnableManager.spawnedObject.transform.position = marker01_02.position;
            spawnableManager.spawnedObject.transform.rotation = marker01_02.rotation;
            objTarget01_02.transform.position = spawnableManager.spawnedObject.transform.position;
            objTarget01_02.transform.rotation = spawnableManager.spawnedObject.transform.rotation;
            StopCoroutine(Count1Min());
            i = 0;
        }
    }
}
