using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Wait : MonoBehaviour
{
    public float wait_time = 8f;

    public int sceneLoad = 1;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Wait_for_load_scene());
    }

    IEnumerator Wait_for_load_scene()
    {
        yield return new WaitForSeconds(wait_time);

        SceneManager.LoadScene(sceneLoad);
    }

    IEnumerator Wait_for_intro()
    {
        yield return new WaitForSeconds(wait_time);

        SceneManager.LoadScene("PopAR");
    }
}
