// using System.Collections;
// using DG.Tweening;
// using TMPro;
// using UnityEngine;
// using UnityEngine.XR.ARFoundation;
// using Google.XR.ARCoreExtensions;
// using Google.XR.ARCoreExtensions.Samples.Geospatial;
// using UnityEngine.XR.ARSubsystems;
// using UnityEngine.Playables;
// using System;

// namespace JohnBui
// {
//     public class MessageController : MonoBehaviour
//     {
//         [SerializeField] ImageTargetController imageTargetController;

//         [SerializeField] ARTrackedImageManager m_TrackedImageManager;


//         [SerializeField] AREarthManager EarthManager;

//         [SerializeField] VpsInitializer Initializer;

//         public bool isTrack1 = false;
//         public bool isTrack2 = false;
//         public bool isTrack3 = false;
//         public bool isActivePopUpSuccessLate = false;

//         public string tracker1 = "track_m1";
//         public string tracker2 = "img-track";
//         public string tracker3 = "banner";

//         private void Start()
//         {
//             isTrack1 = false;
//             isTrack2 = false;
//             isTrack3 = false;
//         }

//         private bool isDetected = true;
//         private bool isActivePopUpMessage = true;

//         public static MessageController Instance;
//         private void Awake()
//         {
//             if (Instance != null && Instance != this)
//             {
//                 Destroy(this);
//             }
//             else
//             {
//                 Instance = this;
//             }
//         }

//         void Update()
//         {
//             DateTime time = DateTime.Now;
//             int currentMin = time.Minute % 10;

//             if (currentMin == 0 && (time.Second >= 0 && time.Second <= 3) && !MessageManager.Instance.isActivePopUpSafe)
//             {
//                 // 0:00 bat lai point to
//                 MessageManager.Instance.isActivePopUpMessage = true;
//             }
//             else if (currentMin == 6 && (time.Second >= 0 && time.Second <= 3) && !MessageManager.Instance.isActivePopUpSafe)
//             {
//                 // 6:00 bat lai point to
//                 MessageManager.Instance.isActivePopUpMessage = true;
//             }

//             if (MessageManager.Instance.isActivePopUpMessage)
//             {
//                 MessageManager.Instance.isActivePopUpMessage = false;
//                 MessageManager.Instance.DeactivePopupFail();
//                 MessageManager.Instance.DeactivePopupSuccess();
//                 MessageManager.Instance.ActivePopupMessage();
//             }



//             if (!Initializer.IsReady || EarthManager.EarthTrackingState != TrackingState.Tracking)
//             {
//                 if (MessageManager.Instance.isCalibFail)
//                 {
//                     MessageManager.Instance.isCalibFail = false;
//                     if (currentMin == 1 && (time.Second >= 0 && time.Second <= 4) && !isTrack1)
//                     {
//                         Debug.Log("ko detect dc m1");
//                         // tu 00:00 den 00:59 khong detect thi 1:00 tat POINT TO bat FAIL
//                         MessageManager.Instance.DeactivePopupMessage();
//                         MessageManager.Instance.DeactivePopupSuccess();
//                         MessageManager.Instance.ActivePopupFail();
//                     }
//                     else if (currentMin == 7 && (time.Second >= 0 && time.Second <= 4) && !isTrack2)
//                     {
//                         Debug.Log("ko detect dc m2");
//                         // tu 06:00 den 06:59 khong detect thi 7:00 tat POINT TO bat FAIL
//                         MessageManager.Instance.DeactivePopupMessage();
//                         MessageManager.Instance.DeactivePopupSuccess();
//                         MessageManager.Instance.ActivePopupFail();

//                     }
//                     else
//                     {
//                         MessageManager.Instance.isCalibFail = true;
//                     }
//                 }
//             }
//             else
//             {

//                 foreach (var trackedImage in m_TrackedImageManager.trackables)
//                 {
//                     if (trackedImage.trackingState == TrackingState.Tracking)
//                     {
//                         if (trackedImage.referenceImage.name == tracker1)
//                         {
//                             isTrack1 = true;
//                         }
//                         if (trackedImage.referenceImage.name == tracker2)
//                         {
//                             isTrack2 = true;
//                         }
//                         if (trackedImage.referenceImage.name == tracker3)
//                         {
//                             isTrack3 = true;
//                         }


//                         // thay hinh anh track
//                         if (MessageManager.Instance.isCalibTrue)
//                         {
//                             MessageManager.Instance.isCalibTrue = false;

//                             // Neu chua hien popupCalibSuccess 
//                             // if(!MessageManager.Instance.isActivePopupCalibSuccess)
//                             // {
//                             //     // trong vong 0:00 den 0:59 hoac 6:00 den 6:59 da detect dc marker ma chua hien popup
//                             //     if ((currentMin == 0 && isTrack1) || (currentMin == 6 && isTrack2))
//                             //     {
//                             //         isActivePopUpSuccessLate = true;
//                             //     }
//                             // }

//                             // if (isTrack3)
//                             // {   
//                             //         // 00:00 den 00:59 neu thanh cong marker 3 tat POINT TO bat SUCCESS
//                             //         // track3Min = currentMin;
//                             //         // track3Sec = time.Sec;
//                             //         MessageManager.Instance.DeactivePopupMessage();
//                             //         MessageManager.Instance.DeactivePopupFail();
//                             //         MessageManager.Instance.ActivePopupSuccess();
//                             //         // MessageManager.Instance.popupCalibFail.DOFade(0f, 0.5f).SetDelay(4f).OnComplete(() => {AactivePopupFail();} );
//                             // } else {
//                             //     MessageManager.Instance.isCalibTrue = true;
//                             // }


//                             if (currentMin == 0 && isTrack1)
//                             {
//                                 // 00:00 den 00:59 neu thanh cong tat POINT TO bat SUCCESS
//                                 isActivePopUpSuccessLate = true;
//                                 MessageManager.Instance.DeactivePopupMessage();
//                                 MessageManager.Instance.DeactivePopupFail();
//                                 MessageManager.Instance.ActivePopupSuccess();
//                             }
//                             else if (currentMin == 6 && isTrack2)
//                             {
//                                 // 06:00 den 06:59 neu thanh cong tat POINT TO bat SUCCESS
//                                 isActivePopUpSuccessLate = true;
//                                 MessageManager.Instance.DeactivePopupMessage();
//                                 MessageManager.Instance.DeactivePopupFail();
//                                 MessageManager.Instance.ActivePopupSuccess();
//                             }
//                             // neu trong vong 1 phut va 7 phut & chua hien SUCCESS & hien muon
//                             else if ((currentMin == 1 || currentMin == 7)
//                             && isActivePopUpSuccessLate && !MessageManager.Instance.isActivePopupCalibSuccess)
//                             {
//                                 // hien muon sau 1 phut va 7 phut
//                                 isActivePopUpSuccessLate = false;
//                                 MessageManager.Instance.DeactivePopupMessage();
//                                 MessageManager.Instance.DeactivePopupFail();
//                                 MessageManager.Instance.ActivePopupSuccess();
//                             }
//                             else
//                             {
//                                 MessageManager.Instance.isCalibTrue = true;
//                             }





//                         }
//                     }
//                 }

//             }
//         }
//     }
// }