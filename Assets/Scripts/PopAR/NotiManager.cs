using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace JohnBui
{
    public class NotiManager : MonoBehaviour
    {
        public GameObject[] notiPopup;
        public bool[] notiPopupStatus;

        public List<NotificationApp> notiList = new List<NotificationApp>();

       
        // Start is called before the first frame update
        void Start()
        {
         loadStart();
            for (int index = 0; index < notiPopup.Length; index++)
            {
                notiPopupStatus[index] = notiPopup[index].active;
            }

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void restartNoti()
        {
            notiList = new List<NotificationApp>();
        }

        public void loadStart()
        {
            Debug.Log("Load start");
            loadNotiByList(new int[] { 3, 4 });
            NotificationApp notificationApp = new NotificationApp(0, new int[] { 3, 4 });
            notiList.Add(notificationApp);
            if (notiList.Contains(notificationApp))
            {
                Debug.Log("Check OK");
            }
        }

        public void loadNotiByList(int[] popup)
        {
            turnOffAll(false);
            foreach (int index in popup)
            {
                notiPopup[index].SetActive(true);
            }
        }

        public void turnOffAll(bool status)
        {
            foreach (GameObject gm in notiPopup)
            {
                gm.SetActive(status);
            }
        }
    }

    public class NotificationApp
    {
        public int time { get; set; }
        public int[] popup { get; set; }

        public NotificationApp(int ptime, int[] ppopup)
        {
            this.time = ptime;
            this.popup = ppopup;
        }
    }
}