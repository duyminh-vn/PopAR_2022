using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JohnBui
{
    public class ReloadScene : MonoBehaviour
    {
        public void reloadScene()
        {
            StartesManagerVer2.Instance.resetStatus();
            ActiveContent.Instance.resetStatus();
            MessageManager.Instance.resetStatus();
        }
    }
}
