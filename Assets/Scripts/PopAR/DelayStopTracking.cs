using UnityEngine;

public class DelayStopTracking : MonoBehaviour
{
    public float delay;

    private float timer;

    private void OnEnable()
    {
        timer = 0;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > delay)
        {
            gameObject.SetActive(false);
        }
    }
}
