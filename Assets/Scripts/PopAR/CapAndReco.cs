using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.ReplayKit;
using VoxelBusters.CoreLibrary;
using UnityEngine.Android;
using System;
using DG.Tweening;
using UnityEngine.EventSystems;


namespace JohnBui
{

public class CapAndReco : MonoBehaviour
{
    public static CapAndReco Instance { get; private set; }

    [SerializeField] private GameObject Logo;

    [SerializeField] private Button BtnBack;

    [SerializeField] private Image imgButton;

    private Color col;

    private bool isFirstTime = true;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
#if PLATFORM_ANDROID
        if (Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
                MessageManager.Instance.ActivePopupSaveAndPhoto();
        }
        else
        {
            Permission.RequestUserPermission(Permission.Camera);
        }
#elif UNITY_IOS
         if (Application.HasUserAuthorization(UserAuthorization.WebCam))
         {
            MessageManager.Instance.ActivePopupSaveAndPhoto();
            ReplayKitManager.StartRecording();
         }
         else 
         {
            Application.RequestUserAuthorization(UserAuthorization.WebCam);
         }
#endif

        col = imgButton.color;
    }

    private void OnEnable()
    {
        //SetStatus("Registered for ReplayKit Callbacks");
        ReplayKitManager.DidInitialise += DidInitialiseEvent;
        ReplayKitManager.DidRecordingStateChange += DidRecordingStateChangeEvent;
    }

    private void OnDisable()
    {
        ReplayKitManager.DidInitialise -= DidInitialiseEvent;
        ReplayKitManager.DidRecordingStateChange -= DidRecordingStateChangeEvent;
    }

    float i = 0;
    bool isCount = true;
    bool isFirstTimeReco = true;

    private IEnumerator CountTime()
    {
        i += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (i > 0.7f)
        {
            if (isFirstTimeReco)
            {
                isCount = false;
                StartRecording();
                yield return new WaitForSeconds(0.5f);
                PointUp();
            }
            else
            {
                isCount = false;
                StartRecording();
            }
        }
        else if (isCount)
        {
            StartCoroutine(CountTime());
        }
    }


    public void PointDown()
    {
        isCount = true;
        StartCoroutine(CountTime());
    }

    public void PointUp()
    {
        if (i < 0.7f)
        {
            isCount = false;
            ScreenShot();
            i = 0;
        }
        else if (i > 0.7f)
        {
            isCount = false;
            StopRecording();
            i = 0;
        }
    }

    private void ScreenShot()
    {
        StartCoroutine(TakeScreenshotAndSave());
    }

    private IEnumerator TakeScreenshotAndSave()
    {
        col.a = 0;
        imgButton.color = col;
        BtnBack.gameObject.SetActive(false);
        Logo.SetActive(true);
        yield return new WaitForEndOfFrame();
        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        col.a = 255;
        imgButton.color = col;
        BtnBack.gameObject.SetActive(true);
        Logo.SetActive(false);
        // Save the screenshot to Gallery/Photos
        NativeGallery.Permission permission = NativeGallery.SaveImageToGallery(ss, "GalleryTest", "Image.png",
            (success, path) => Debug.Log("Media save result: " + success + " " + path));

        Debug.Log("Permission result: " + permission);

        // To avoid memory leaks
        Destroy(ss);
    }

    private void SetMicrophoneStatus()
    {
        ReplayKitManager.SetMicrophoneStatus(true);
    }

    private void StartRecording()
    {
        Logo.SetActive(true);
        BtnBack.gameObject.SetActive(false);
        col.a = 0;
        imgButton.color = col;
        ReplayKitManager.StartRecording();
    }

    private void StopRecording()
    {
        ReplayKitManager.StopRecording((result, error) =>
        {
            if (error == null)
            {
                Debug.Log($"File path: {result.Path}");
                Logo.SetActive(false);
                BtnBack.gameObject.SetActive(true);
                col.a = 255;
                imgButton.color = col;
                if (!isFirstTimeReco)
                {
                    SavePreview();
                }
                else
                {
                    Discard();
                    isFirstTimeReco = false;
                }
            }
            else
            {
                Debug.Log($"Failed with error: {error}");
            }
        });
    }

    private void SavePreview() //Saves preview to gallery
    {
        if (ReplayKitManager.IsPreviewAvailable())
        {
            ReplayKitManager.SavePreview((result, error) =>
            {
                if (error == null)
                {
                    //SetStatus($"Saved preview to gallery.");
                    Discard();
                }
                else
                {
                    //SetStatus($"Saved preview to gallery failed with error: {error}");
                }
            });
        }
        else
        {
            //SetStatus("Preview recording not available. If you have already recoded, wait for ReplayKitRecordingState.Available status event and try saving again.");
        }
    }

    private void Discard()
    {
        bool success = ReplayKitManager.Discard();
        //SetStatus($"Discard  recording: {(success ? "Success" : "Failed")}");
    }

    private void DidInitialiseEvent(InitialiseCompleteResult result, Error error)
    {
        Debug.Log("Received Event Callback : DidInitialiseEvent [State:" + result.State.ToString() + " " + "Error:" +
                  error);

        switch (result.State)
        {
            case ReplayKitInitialisationState.Success:
                //SetStatus("ReplayKitManager.DidInitialiseEvent : Initialisation Success");
                break;

            case ReplayKitInitialisationState.Failed:
                //SetStatus("ReplayKitManager.DidInitialiseEvent : Initialisation Failed with message[" + error + "]");
                break;

            default:
                //SetStatus("Unknown State");
                break;
        }
    }

    private void DidRecordingStateChangeEvent(RecordingStateChangeResult result, Error error)
    {
        Debug.Log("Received Event Callback : DidRecordingStateChangeEvent [State:" + result.State.ToString() + " " +
                  "Error:" + error);

        switch (result.State)
        {
            case ReplayKitRecordingState.Started:
                //SetStatus("ReplayKitManager.DidRecordingStateChangeEvent : Video Recording Started");
                break;

            case ReplayKitRecordingState.Stopped:
                //SetStatus("ReplayKitManager.DidRecordingStateChangeEvent : Video Recording Stopped");
                break;

            case ReplayKitRecordingState.Failed:
                //SetStatus("ReplayKitManager.DidRecordingStateChangeEvent : Video Recording Failed with message [" + error + "]");
                break;

            case ReplayKitRecordingState.Available:
                //SetStatus("ReplayKitManager.DidRecordingStateChangeEvent : Video Recording available for preview");
                break;

            default:
                //SetStatus("Unknown State");
                break;
        }
    }
}
}