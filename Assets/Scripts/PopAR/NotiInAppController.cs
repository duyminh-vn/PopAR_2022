using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;
using Google.XR.ARCoreExtensions;


namespace JohnBui
{
    public class NotiInAppController : MonoBehaviour
    {

        [SerializeField] ARTrackedImageManager m_TrackedImageManager;

        public string tracker1 = "track_m1";
        public string tracker2 = "img-track";
        public string tracker3 = "banner";

        public bool isTrack1 = false;
        public bool isTrack2 = false;
        public bool isTrack3 = false;
        public bool isActivePopUpSuccessLate = false;


        public Animator eventAnimator;
        public int startedTime = 0;

        // Start is called before the first frame update
        void Start()
        {
            eventAnimator.Play("open_app");
            DateTime now = DateTime.Now;
            startedTime = now.Minute % 10;
        }

        // Update is called once per frame
        void Update()
        {
            DateTime time = DateTime.Now;
            int currentMin = time.Minute % 10;


            //Foreach
            foreach (var trackedImage in m_TrackedImageManager.trackables)
            {
                if (trackedImage.trackingState == TrackingState.Tracking)
                {
                    if (trackedImage.referenceImage.name == tracker1)
                    {
                        if (currentMin == 0) isTrack1 = true;
                        if (isTrack3) eventAnimator.Play("calibSucess1");
                    }
                    if (trackedImage.referenceImage.name == tracker2)
                    {
                        if (currentMin == 6) isTrack2 = true;
                        if(isTrack3) eventAnimator.Play("calibSucess2");
                    }
                    if (trackedImage.referenceImage.name == tracker3)
                    {
                        isTrack3 = true;
                    }
                }

                //Foreach
            }

            /* openApp
                  calibMess
                  calibSucess
                  calibFail
                  offAll
                  */

            //LOGIC:
            if (isTrack3 && !isTrack1 && !isTrack2)
            {
                //eventAnimator.Play("Init");
                eventAnimator.Play("calibSucess");
            }
            else
            {

                if ((currentMin == 0 && isTrack1) || (currentMin == 1 && time.Second < 5 && isTrack1))
                {
                    // 00:00 den 00:59 neu thanh cong tat POINT TO bat SUCCESS
                  
                    eventAnimator.Play("calibSucess1");
                    isActivePopUpSuccessLate = true;
                }
                else if (currentMin == 6 && isTrack2)
                {
                    // 06:00 den 06:59 neu thanh cong tat POINT TO bat SUCCESS
                   
                    eventAnimator.Play("calibSucess2");
                    isActivePopUpSuccessLate = true;
                }
                // neu trong vong 1 phut va 7 phut & chua hien SUCCESS & hien muon
                else if (((currentMin == 1 && time.Second < 5) || (currentMin == 7 && time.Second < 5))
                && isActivePopUpSuccessLate)
                {
                    // hien muon sau 1 phut va 7 phut
                   
                    //eventAnimator.Play("offAll");
                    eventAnimator.Play("calibSucess2");
                }


                if (currentMin == 6 && time.Second == 0)
                {
                    resetStatus();
                }

                //if ((currentMin == 1 && time.Second > 5 && !isTrack1 && startedTime == 0))
                //{
                //    eventAnimator.Play("calibFail");
                //}
                //else if ((currentMin == 1 && time.Second > 5 && isTrack1))
                //{
                //    eventAnimator.Play("offAll");
                //}

                //if ((currentMin == 7 && time.Second > 5 && !isTrack2 && startedTime != 7))
                //{
                //    eventAnimator.Play("calibFail");
                //}
                //else if ((currentMin == 7 && time.Second > 5 && isTrack2))
                //{
                //    eventAnimator.Play("offAll");
                //}




                if ((currentMin == 1 && time.Second > 5 && !isTrack1 && startedTime != 1))
                {
                    eventAnimator.Play("calibFail");
                }
                else if ((currentMin <= 5 && (currentMin > 1 && time.Second > 5) && isTrack1))
                {
                    eventAnimator.Play("Init");
                }

                if ((currentMin == 7 && time.Second > 5 && !isTrack2 && startedTime != 7))
                {
                    eventAnimator.Play("calibFail");
                }
                else if ((currentMin <= 9 && (currentMin > 7 && time.Second > 5) && isTrack2))
                {
                    eventAnimator.Play("Init");
                }


                //if (currentMin == 6 && !isTrack2 && startedTime != 6)
                //{
                //    eventAnimator.Play("calibMessLoop");
                //}

                //if ((currentMin == 7 && time.Second > 5 && !isTrack2 && startedTime != 7))
                //{
                //    eventAnimator.Play("calibFail");
                //}
                //if ((currentMin == 8 && time.Second > 5 && !isTrack2 && startedTime != 8))
                //{
                //    eventAnimator.Play("calibFail");
                //}
                //if ((currentMin == 9 && time.Second > 5 && !isTrack2 && startedTime != 9))
                //{
                //    eventAnimator.Play("calibFail");
                //}
                //else if ((currentMin == 7 && time.Second < 5 && isTrack2))
                //{
                //    eventAnimator.Play("Init");
                //}

                if (currentMin == 9 && time.Second == 59)
                {
                    isTrack1 = false;
                    isTrack2 = false;
                    isTrack3 = false;
                    isActivePopUpSuccessLate = false;
                    eventAnimator.Play("calibMessLoop");
                }


            }     
        }
        public void resetStatus()
        {
            isTrack1 = false;
            isTrack2 = false;
            isTrack3 = false;
            isActivePopUpSuccessLate = false;
            eventAnimator.Play("calibMessLoop");
        }

    }
}
