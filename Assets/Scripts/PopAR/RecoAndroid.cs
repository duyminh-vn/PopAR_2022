using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


namespace JohnBui
{

public class RecoAndroid : MonoBehaviour
{
    bool isPressed = true;
    public void OnUpdateSelected(BaseEventData data)
    {
        if (isPressed)
        {
            CapAndReco.Instance.PointDown();
        }
        else
        {
            CapAndReco.Instance.PointUp();
        }
    }
    public void OnPointerDown(PointerEventData data)
    {
        isPressed = true;
    }
    public void OnPointerUp(PointerEventData data)
    {
        isPressed = false;
    }
}
}
