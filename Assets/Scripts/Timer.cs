using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public static float initialTimeValue = 90;
    public float timeValue = initialTimeValue;
    public TextMesh timeText;
    // public TextScript timeTextScript;

    // Update is called once per frame
    void Update()
    {
        if (timeValue > 0)
        {
            timeValue -= Time.deltaTime;
        } 
        else
        {
            timeValue += initialTimeValue;
        }
        DisplayTime(timeValue);
    }

    void DisplayTime(float timeToDisplay)
    {
        if(timeToDisplay < 0)
        {
            timeToDisplay = 0;
        }

        int seconds = Mathf.FloorToInt(timeToDisplay);
        timeText.text = string.Format("{0}", seconds);
        // timeTextScript.EnterTextHere = string.Format("{0}", seconds);

    }
}
