using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Playables;

namespace JohnBui
{
    public class StartesManagerVer2 : MonoBehaviour
    {

        [SerializeField] ARTrackedImageManager m_TrackedImageManager;

        private Transform preTransform;
        public static StartesManagerVer2 Instance;

        public bool isTrackedMark1 = false, isTrackedMark2 = false, isTrackedMark3 = false;
        private bool isDetectedPreshow = false, isDetectedBMW = false, isDetectedFW = false;
 
        private float countdownTimeFX= 0.033f;
        private float timeActiveFX = 0.033f;

        DateTime now = DateTime.Now;
        public string trackImg1 = "pop_marker";
        public string trackImg2 = "bmw_marker";
        public string trackImg3 = "banner";

        bool isConnect = true;
        

        // private void Start()
        // {
        //     mocktCheckFW = false;
        //     tapMockt = 0;
        //     resetStatus();
        //     // gmShow03.SetActive(false);

        //     //StartCoroutine(checkInternetConnection());

        //     //DateTime currentDateTime = WorldTimeAPI.Instance.GetCurrentDateTime();
        //     //Debug.Log(currentDateTime.Hour + " : " + currentDateTime.Minute + " : " + currentDateTime.Second);
        // }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        public void resetStatus()
        {
            preTransform = null;
            isTrackedMark1 = false;
            isTrackedMark2 = false;
            isTrackedMark3 = false;

            isDetectedPreshow = false;
            isDetectedBMW = false;
            isDetectedFW = false;
 
            countdownTimeFX= 0.033f;
            timeActiveFX = 0.033f;

            isConnect = true;
        }

        private void Update()
        {
            now = DateTime.Now;
            int minPer10min = now.Minute % 10;

            foreach (var trackedImage in m_TrackedImageManager.trackables)
            {
                if (trackedImage.trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Tracking)
                {            
                    if (trackedImage.referenceImage.name == trackImg1)
                    {
                        isTrackedMark1 = true;
                    }  else {
                        isTrackedMark1 = false;
                    }
                    if (trackedImage.referenceImage.name == trackImg2)
                    {
                        
                        isTrackedMark2 = true;
                    }  else {
                        isTrackedMark2 = false;
                    }
                    if (trackedImage.referenceImage.name == trackImg3 && (!isTrackedMark1 || !isTrackedMark2))
                    {
                        isTrackedMark3 = true;
                    }   else {
                        isTrackedMark3 = false;
                    }  
                    Debug.Log("Track "+ now +" mk1: " + isTrackedMark1 + " mk2: " + isTrackedMark2 + " mk3: " + isTrackedMark3);
                    // 23:50:00 den 23:59:59
                    if (now.Hour == 23 && now.Minute >= 50)
                    {
                        // 23:50:00 den 23:50:59
                        if (now.Minute == 50)
                        {
                            if(isTrackedMark1) 
                            {   
                                SetPreshowTransform(trackedImage.transform);
                            } 
                            else if(isTrackedMark3)
                            {
                                SetKVTransform(trackedImage.transform);
                            }
                        } 
                        // 23:51:00 den 23:51:03
                        else if (now.Minute == 51 && now.Second <= 3)
                        {
                            if(isDetectedPreshow)
                            {
                                if(preTransform.position != new Vector3(0,0,0))
                                {
                                    ActiveContent.Instance.ActiveMainShow(preTransform);
                                }
                                isDetectedPreshow = false;
                            } 
                            else if(isTrackedMark3)
                            {
                                SetKVTransform(trackedImage.transform);
                            } else if(!ActiveContent.Instance.MainShow.activeInHierarchy
                            && !ActiveContent.Instance.KV.activeInHierarchy && !isTrackedMark3) 
                            {
                                Debug.Log("un track show, detectpreshow "+ isDetectedPreshow);
                                MessageManager.Instance.ActivePopupFail();
                            }
                        }
                        else if (now.Minute >= 59 )
                        {
                            if(isTrackedMark1) 
                            {   
                                if (ActiveContent.Instance.KV.activeInHierarchy)
                                {
                                    ActiveContent.Instance.InActiveKV();
                                }
                                if (!ActiveContent.Instance.isShowedSuccess)
                                {
                                    MessageManager.Instance.ActivePopupSuccess();
                                    if(MessageManager.Instance.popupSuccess.gameObject.activeInHierarchy)
                                    {
                                        ActiveContent.Instance.isShowedSuccess = true;
                                    }
                                }
                                isDetectedFW = true;
                                preTransform = trackedImage.transform;
                            } 
                            else if(isTrackedMark3)
                            {
                                SetKVTransform(trackedImage.transform);
                            }
                        }
                        else 
                        {
                            if(isTrackedMark3)
                            {
                                SetKVTransform(trackedImage.transform);
                            }
                        }
                    }
                    // 00:00:00 den 00:09:59
                    else if (now.Hour == 0 && now.Minute < 10)
                    {
                        if(MessageManager.Instance.popupPointTo.gameObject.activeInHierarchy)
                        {
                            MessageManager.Instance.DeactivePopupPointTo();
                        }
                        ActiveContent.Instance.ActiveShow10minFireWork(preTransform);
                        isDetectedFW = false;
                    } 
                    else
                    {
                        // 00:00 den 00:59
                        if (minPer10min == 0)
                        {
                            if(isTrackedMark1) 
                            {   
                                SetPreshowTransform(trackedImage.transform);
                            } else if(isTrackedMark3)
                            {
                                SetKVTransform(trackedImage.transform);
                                Debug.Log("Track show 1 "+ isDetectedPreshow);
                            }
                        } 
                        // 01:00 den 01:03
                        else if (minPer10min == 1 && now.Second <= 3)
                        {
                            Debug.Log("1:00 Track show 1 "+ isDetectedPreshow);
                            if(isDetectedPreshow)
                            {
                                Debug.Log("Track show 1 "+ isDetectedPreshow);
                                if(preTransform.position != new Vector3(0,0,0))
                                {
                                    ActiveContent.Instance.ActiveMainShow(preTransform);
                                }
                                isDetectedPreshow = false;
                            } else if(isTrackedMark3)
                            {
                                Debug.Log("Track banner "+ isTrackedMark3);
                                SetKVTransform(trackedImage.transform);
                            } 
                            else if(!ActiveContent.Instance.MainShow.activeInHierarchy
                            && !ActiveContent.Instance.KV.activeInHierarchy && !isTrackedMark3) 
                            {
                                Debug.Log("un track show, detectpreshow "+ isDetectedPreshow);
                                MessageManager.Instance.ActivePopupFail();
                            }
                        }
                        // 06:00 den 06:59
                        else if (minPer10min == 6 )
                        {
                            if(isTrackedMark2) 
                            {   
                                if (ActiveContent.Instance.KV.activeInHierarchy)
                                {
                                    ActiveContent.Instance.InActiveKV();
                                }
                                if (!ActiveContent.Instance.isShowedSuccess)
                                {
                                    MessageManager.Instance.ActivePopupSuccess();
                                    if(MessageManager.Instance.popupSuccess.gameObject.activeInHierarchy)
                                    {
                                        ActiveContent.Instance.isShowedSuccess = true;
                                    }
                                }
                                isDetectedBMW = true;
                                Debug.Log("Track marker 2 " + isDetectedBMW);
                                preTransform = trackedImage.transform;
                            } else if(isTrackedMark3)
                            {
                                SetKVTransform(trackedImage.transform);
                                Debug.Log("Track banner " + isDetectedBMW);
                            }
                        }
                        // 07:00 den 07:03
                        else if (minPer10min == 7 && now.Second <= 10)
                        {
                            Debug.Log("7 min Track BMW "+isDetectedBMW);
                            if(isDetectedBMW)
                            {
                                Debug.Log("Track BMW "+isDetectedBMW);
                                if(preTransform.position != new Vector3(0,0,0))
                                {
                                    ActiveContent.Instance.ActiveCrystal(preTransform);
                                }
                                isDetectedBMW = false;
                            } else if(isTrackedMark3)
                            {
                                Debug.Log("Track banner " + isDetectedBMW);
                                SetKVTransform(trackedImage.transform);
                            } 
                            else if(!ActiveContent.Instance.Crystal.activeInHierarchy
                            && !ActiveContent.Instance.KV.activeInHierarchy &&!isTrackedMark3) 
                            {
                                Debug.Log("un Track marker 2 " + isDetectedBMW);
                                MessageManager.Instance.ActivePopupFail();
                            }
                        }
                        // 9:57 den 9:59 reset bien preTrasform
                        else if (minPer10min == 9 && now.Second >= 57)
                        {
                            preTransform = null;
                        }
                        else 
                        {
                            if(isTrackedMark3)
                            {
                                SetKVTransform(trackedImage.transform);
                            }
                        }
                    }
                } else 
                {
                    if (now.Hour == 23 && now.Minute >= 50)
                    {
                        // 23:51:00 den 23:51:03
                        if (now.Minute == 51 && now.Second <= 3)
                        {
                            if(isDetectedPreshow)
                            {
                                if(preTransform.position != new Vector3(0,0,0))
                                {
                                    ActiveContent.Instance.ActiveMainShow(preTransform);
                                }
                                isDetectedPreshow = false;
                            } 
                            else  if(!ActiveContent.Instance.MainShow.activeInHierarchy &&!isTrackedMark3) 
                            {
                                MessageManager.Instance.ActivePopupFail();
                            }
                        }
                    }
                    // 00:00:00 den 00:09:59
                    else if (now.Hour == 0 && now.Minute >= 0 && now.Minute < 10)
                    {
                        if(MessageManager.Instance.popupPointTo.gameObject.activeInHierarchy)
                        {
                            MessageManager.Instance.DeactivePopupPointTo();
                        }
                        ActiveContent.Instance.ActiveShow10minFireWork(preTransform);
                        isDetectedFW = false;
                    } 
                    else
                    {
                        // 01:00 den 01:03
                        if (minPer10min == 1 && now.Second <= 3)
                        {
                            Debug.Log("1:00 Track show 1 "+ isDetectedPreshow);
                            if(isDetectedPreshow)
                            {
                                if(preTransform.position != new Vector3(0,0,0))
                                {
                                    ActiveContent.Instance.ActiveMainShow(preTransform);
                                }
                                isDetectedPreshow = false;
                            } 
                            else if(!ActiveContent.Instance.MainShow.activeInHierarchy
                            && !ActiveContent.Instance.KV.activeInHierarchy && !isTrackedMark3) 
                            {
                                Debug.Log("un track show, detectpreshow "+ isDetectedPreshow);
                                MessageManager.Instance.ActivePopupFail();
                            }
                        }
                        // 07:00 den 07:03
                        else if (minPer10min == 7 && now.Second <= 10)
                        {
                            Debug.Log("7 min Track BMW "+isDetectedBMW);
                            if(isDetectedBMW)
                            {
                                Debug.Log("Track BMW "+isDetectedBMW);
                                if(preTransform.position != new Vector3(0,0,0))
                                {
                                    ActiveContent.Instance.ActiveCrystal(preTransform);
                                }
                                isDetectedBMW = false;
                            } else if(isTrackedMark3)
                            {
                                Debug.Log("Track banner " + isDetectedBMW);
                                SetKVTransform(trackedImage.transform);
                            } 
                            else if(!ActiveContent.Instance.Crystal.activeInHierarchy
                            && !ActiveContent.Instance.KV.activeInHierarchy &&!isTrackedMark3) 
                            {
                                Debug.Log("un Track marker 2 " + isDetectedBMW);
                                MessageManager.Instance.ActivePopupFail();
                            }
                        }
                        // 9:57 den 9:59 reset bien preTrasform
                        else if (minPer10min == 9 && now.Second >= 57)
                        {
                            preTransform = null;
                        }
                    }
                }
            }
        }    
        public void SetPreshowTransform(Transform transform)
        {
            isDetectedPreshow = true;
            Debug.Log("Track preshow"+isDetectedPreshow);
            preTransform = transform;

            if(!ActiveContent.Instance.lockedFX)
            {
                ActiveContent.Instance.ActivePreShow();
                ActiveContent.Instance.PreShow.transform.position = transform.position;
                ActiveContent.Instance.PreShow.transform.rotation = transform.rotation;
            } else
            {
                countdownTimeFX -= Time.unscaledDeltaTime;
                ActiveContent.Instance.ActivePreShow();
                if (countdownTimeFX <=0)
                {
                    countdownTimeFX = timeActiveFX;
                    ActiveContent.Instance.PreShow.transform.position = Vector3.Lerp(ActiveContent.Instance.PreShow.transform.position, transform.position, timeActiveFX);
                    ActiveContent.Instance.PreShow.transform.rotation = Quaternion.Lerp(ActiveContent.Instance.PreShow.transform.rotation, transform.rotation, timeActiveFX);
                }
            }

        }     
        public void SetKVTransform(Transform transform)
        {
            if (!ActiveContent.Instance.PreShow.activeInHierarchy
                && !ActiveContent.Instance.MainShow.activeInHierarchy
                && !ActiveContent.Instance.Crystal.activeInHierarchy
                && !ActiveContent.Instance.Show10minFireWork.activeInHierarchy
                && !isDetectedBMW && !isDetectedFW && !isDetectedPreshow)
            {    
                if(!ActiveContent.Instance.lockedFX)
                {
                    ActiveContent.Instance.ActiveKV();
                    ActiveContent.Instance.KV.transform.position = transform.position;
                    ActiveContent.Instance.KV.transform.rotation = transform.rotation;
                } else
                {
                    Debug.Log("Track banner");
                    countdownTimeFX -= Time.unscaledDeltaTime;
                    ActiveContent.Instance.ActiveKV();
                    if (countdownTimeFX <=0)
                    {
                        countdownTimeFX = timeActiveFX;
                        ActiveContent.Instance.KV.transform.position = Vector3.Lerp(ActiveContent.Instance.KV.transform.position, transform.position, timeActiveFX);
                        ActiveContent.Instance.KV.transform.rotation = Quaternion.Lerp(ActiveContent.Instance.KV.transform.rotation, transform.rotation, timeActiveFX);
                    }
                }
            }

        }
        IEnumerator checkInternetConnection()
        {
            WWW www = new WWW("http://google.com");
            yield return www;
            if (www.error != null)
            {
                isConnect = false;
            }
            else
            {
                isConnect = true;
            }
        }    
    }
}