using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Playables;

namespace JohnBui
{
    public class ActiveContent : MonoBehaviour
    {
        public GameObject PreShow, MainShow, Crystal, KV, Show10minFireWork;

        public static ActiveContent Instance;

        public float mainShowLifeTime = 300f, crystalLifeTime = 180f, KVLifeTime = 60f, show10minFireWorkLifeTime = 600f;

        private float timeFx = 0f, countdownLockTime = 0f, lockTime = 0.1f;
        public bool lockedFX = false;
        public bool isShowedSuccess = false;

        DateTime now = DateTime.Now;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }
        public void resetStatus()
        {
            PreShow.SetActive(false);
            MainShow.SetActive(false);
            Crystal.SetActive(false);
            KV.SetActive(false);
            Show10minFireWork.SetActive(false);
            timeFx = 0f;
            countdownLockTime = 0f;
            lockedFX = false;
            isShowedSuccess = false;
        }

        private void Start()
        {
            PreShow.SetActive(false);
            MainShow.SetActive(false);
            Crystal.SetActive(false);
            KV.SetActive(false);
            Show10minFireWork.SetActive(false);
            timeFx = 0f;
            countdownLockTime = 0f;
            lockedFX = false;
            isShowedSuccess = false;
        }

        private void Update()
        {
            now = DateTime.Now;
            if (PreShow.activeInHierarchy)
            {
                countdownLockTime += Time.unscaledDeltaTime;
                if(countdownLockTime >= lockTime && now.Minute == 0 && now.Second <59)
                {
                    lockedFX = true;
                }
                if ( now.Minute == 0 && now.Second >=59)
                {
                    lockedFX = false;
                    countdownLockTime = 0;
                }
            } 
            if (MainShow.activeInHierarchy)
            {
                timeFx += Time.unscaledDeltaTime;
                if(timeFx >= mainShowLifeTime)
                {
                    InActiveMainShow();
                    timeFx = 0;
                }
            }
            if (Crystal.activeInHierarchy)
            {
                timeFx += Time.unscaledDeltaTime;
                if(timeFx >= crystalLifeTime)
                {
                    InActiveCrystal();
                    timeFx = 0;
                }
            }
            if (KV.activeInHierarchy)
            {
                timeFx += Time.unscaledDeltaTime;
                countdownLockTime += Time.unscaledDeltaTime;
                if(countdownLockTime >= lockTime && countdownLockTime < KVLifeTime)
                {
                    lockedFX = true;
                }
                if(timeFx >= KVLifeTime)
                {
                    InActiveKV();
                    lockedFX = false;
                    countdownLockTime = 0;
                    timeFx = 0;
                }
            }
            if (Show10minFireWork.activeInHierarchy)
            {
                timeFx += Time.unscaledDeltaTime;
                if(timeFx >= show10minFireWorkLifeTime)
                {
                    InActiveShow10minFireWork();
                    timeFx = 0;
                }
            }
        }

        public void ActivePreShow()
        {
            if (MainShow.activeInHierarchy)
            {
                InActiveMainShow();
            }
            if (Crystal.activeInHierarchy)
            {
                InActiveCrystal();
            }
            if (KV.activeInHierarchy)
            {
                InActiveKV();
            }
            if (Show10minFireWork.activeInHierarchy)
            {
                InActiveShow10minFireWork();
            } 
            if (!isShowedSuccess)
            {
                MessageManager.Instance.ActivePopupSuccess();
                if(MessageManager.Instance.popupSuccess.gameObject.activeInHierarchy)
                {
                    isShowedSuccess = true;
                }
            }
            PreShow.SetActive(true);
            // PreShow.transform.position = transform.position;
            // PreShow.transform.rotation = transform.rotation;
            
        }
        public void ActiveMainShow(Transform transform)
        {
            
            if (PreShow.activeInHierarchy)
            {
                InActivePreShow();
            }
            if (Crystal.activeInHierarchy)
            {
                InActiveCrystal();
            }
            if (KV.activeInHierarchy)
            {
                InActiveKV();
            }
            if (Show10minFireWork.activeInHierarchy)
            {
                InActiveShow10minFireWork();
            }
            MainShow.SetActive(true);
            MainShow.transform.position = transform.position;
            MainShow.transform.rotation = transform.rotation;
            
        }
        public void ActiveCrystal(Transform transform)
        {
            if (PreShow.activeInHierarchy)
            {
                InActivePreShow();
            }
            if (MainShow.activeInHierarchy)
            {
                InActiveMainShow();
            }
            if (KV.activeInHierarchy)
            {
                InActiveKV();
            }
            if (Show10minFireWork.activeInHierarchy)
            {
                InActiveShow10minFireWork();
            }
            Crystal.SetActive(true);
            Crystal.transform.position = transform.position;
            Crystal.transform.rotation = transform.rotation;
            
        }
        public void ActiveKV()
        {
            if (!MainShow.activeInHierarchy && !Crystal.activeInHierarchy)
            {
                if (!isShowedSuccess)
                {
                    MessageManager.Instance.ActivePopupSuccess();
                    if(MessageManager.Instance.popupSuccess.gameObject.activeInHierarchy)
                    {
                        isShowedSuccess = true;
                    }
                }
                KV.SetActive(true);
            }
        }
        public void ActiveShow10minFireWork(Transform transform)
        {
            
            if (PreShow.activeInHierarchy)
            {
                InActivePreShow();
            }
            if (MainShow.activeInHierarchy)
            {
                InActiveMainShow();
            }
            if (Crystal.activeInHierarchy)
            {
                InActiveCrystal();
            }
            if (KV.activeInHierarchy)
            {
                InActiveKV();
            }
            Show10minFireWork.SetActive(true);
            Show10minFireWork.transform.position = transform.position;
            Show10minFireWork.transform.rotation = transform.rotation;
        }

        public void InActivePreShow()
        {
            PreShow.SetActive(false);
        }
        public void InActiveMainShow()
        {
            isShowedSuccess = false;
            MainShow.SetActive(false);
            MessageManager.Instance.ActivePopupPointTo();
        }
        public void InActiveCrystal()
        {
            isShowedSuccess = false;
            Crystal.SetActive(false);
            MessageManager.Instance.ActivePopupPointTo();
        }
        public void InActiveKV()
        {
            isShowedSuccess = false;
            KV.SetActive(false);
            if(!StartesManagerVer2.Instance.isTrackedMark1 && !StartesManagerVer2.Instance.isTrackedMark2)
            {
                MessageManager.Instance.ActivePopupPointTo();
            }
        }
        public void InActiveShow10minFireWork()
        {
            isShowedSuccess = false;
            Show10minFireWork.SetActive(false);
            MessageManager.Instance.ActivePopupPointTo();
        }
    }
}
