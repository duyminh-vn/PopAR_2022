using System;
using Google.XR.ARCoreExtensions;
using Google.XR.ARCoreExtensions.Samples.Geospatial;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;
namespace JohnBui
{
    public class TrackingMonitor : MonoBehaviour
    {
        
        [SerializeField] AREarthManager EarthManager;
      
        [SerializeField] VpsInitializer Initializer;
      
        [SerializeField] Text OutputText;
      
        [SerializeField] double HeadingThreshold = 25;
      
        [SerializeField] double HorizontalThreshold = 20;

        // Update is called once per frame
        void Update()
        {        
         
            if (!Initializer.IsReady || EarthManager.EarthTrackingState != TrackingState.Tracking)
            {
                return;
            }
       
            string status = "";
         
            GeospatialPose pose = EarthManager.CameraGeospatialPose;
          
            if (pose.HeadingAccuracy > HeadingThreshold ||
                 pose.HorizontalAccuracy > HorizontalThreshold)
            {
                status = "Low tracking";
            }
            else 
            {
                status = "High Tracking Accuracy";
            }
           
            ShowTrackingInfo(status, pose);
        }

        void ShowTrackingInfo(string status, GeospatialPose pose)
        {
            if (OutputText == null) return;
            OutputText.text = string.Format(
                "\n" +
                "Latitude/Longitude: {0}°, {1}°\n" +
                "Horizontal Accuracy: {2}m\n" +
                "Altitude: {3}m\n" +
                "Vertical Accuracy: {4}m\n" +
                "Heading: {5}°\n" +
                "Heading Accuracy: {6}°\n" +
                "{7} \n"
                ,
                pose.Latitude.ToString("F6"),  //{0}
                pose.Longitude.ToString("F6"), //{1}
                pose.HorizontalAccuracy.ToString("F6"), //{2}
                pose.Altitude.ToString("F2"),  //{3}
                pose.VerticalAccuracy.ToString("F2"),  //{4}
                pose.Heading.ToString("F1"),   //{5}
                pose.HeadingAccuracy.ToString("F1"),   //{6}
                status //{7}
            );
        }
    }
}